package validators;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class DateValidator implements Validator {

	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		// TODO Auto-generated method stub
		
		//hacemos un casting y comprobar que la fecha no es superior a la de hoy
		Date fecha =(Date) arg2;
		if(fecha.compareTo(new Date())<0) {
			FacesMessage mensaje = new FacesMessage("Error en la validacion: la fecha debe de ser posterior a hoy");
			mensaje.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(mensaje);
		}

	}

}
