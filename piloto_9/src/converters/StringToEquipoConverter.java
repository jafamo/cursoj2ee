package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import dao.DAOFactory;
import domain.Equipo;

public class StringToEquipoConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		// TODO Auto-generated method stub
		//args2 es el id del 
		Equipo equipo = null;
		//hacemos un casting del string arg2
		Integer id = Integer.valueOf(arg2);
		//devuelve el equipo a través del id que ha encontrado
		DAOFactory.getEquipoDAO().findById(id);
		
		return equipo;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		// TODO Auto-generated method stub
		
		return arg2.toString();
		//return null;
	}

}
