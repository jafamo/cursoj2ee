/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import dao.DAOFactory;
import dao.utils.HibernateUtil;
import domain.Partido;

@ManagedBean
@SessionScoped
public class PartidoBean {

    
    
 //**************************** TODO **********************************************   
 //   Implementar un actionListener que llame al m�todo crearPartido
 //   Enlazar con bot�n Nuevo de listPartidos.xhtml
 //********************************************************************************
    
	public void crearPartidoListener(ActionEvent ae) {
		crearPartido();
    }

    
    private void crearPartido(){
    	partido=new Partido();
    }
    
  
  //**************************** TODO **************************************
  //   Implementar un changeValueListener que llame al m�todo filtraPorJornada
  //   Enlazar con selectOneMenu del filtro por jornada de listPartidos.xhtml
  //************************************************************************
    
    public void filtraListener(ValueChangeEvent ve) {
    	filtraPorJornada((String)ve.getNewValue());
    }
 
    
    private void filtraPorJornada(String jornada){
    	if(jornada.equals("Todas"))
    		listaDePartidos=DAOFactory.getPartidoDAO().findAll();
    	else
    		listaDePartidos=DAOFactory.getPartidoDAO().findByJornada(jornada);
    }
    
    
    
  //**************************** TODO **************************************
  //   Implementar un actionListener que llame al m�todo filtraPorEquipo
  //   Enlazar con bot�n OK del filtro por equipo de listPartidos.xhtml
  //************************************************************************
    
    
    public void filtraPorEquipoListener(ActionEvent ae) {
    	filtraPorEquipo();
    }
    
    private void filtraPorEquipo(){
    	if(equipo.trim().length()==0)
    		listaDePartidos=DAOFactory.getPartidoDAO().findAll();
    	else
    		listaDePartidos=DAOFactory.getPartidoDAO().findByEquipo(equipo);
    }
        
    public String equipo;
    public String getEquipo(){return equipo;}
	public void setEquipo(String equipo){this.equipo = equipo;}

	
	//**************************** TODO **********************************************
	//   Implementar un actionListener que llame al m�todo guardar
	//   Enlazar con bot�n Guardar de editPartido.xhtml
	//********************************************************************************
	     
	public void guardarListener(ActionEvent ae) {
		guardar();
	}
	   
	   private void guardar(){
		   HibernateUtil.beginTransaction();
		   DAOFactory.getPartidoDAO().save(partido);
		   HibernateUtil.endTransaction();
		   dirty=true;
	   }
	
	
	   
	   
	       
//********************************************************************************       
// Resto de atributos, getters y setters
//********************************************************************************
	      
	private Partido partido;
	public Partido getPartido() {
        return partido;
    }
    public void setPartido(Partido partido) {
        this.partido = partido;
    }


    private List<Partido> listaDePartidos;
    private boolean dirty=false;
    public List<Partido> getListaDePartidos() {
        if(listaDePartidos==null || dirty==true){
            listaDePartidos=DAOFactory.getPartidoDAO().findAll();
            dirty=false;
        }
        return listaDePartidos;
    }
    public void setListaDePartidos(List<Partido> partidos) {
        this.listaDePartidos = partidos;
    }


    private boolean porJugar;
	public boolean isPorJugar() {
		return porJugar;
	}
	public void setPorJugar(boolean porJugar) {
		this.porJugar = porJugar;
	}
    

}
