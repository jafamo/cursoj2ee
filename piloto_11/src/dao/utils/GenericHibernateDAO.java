package dao.utils;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;


public abstract class GenericHibernateDAO{

	private Session session;


	public GenericHibernateDAO(Session s){
		session=s;
	}
	
	@SuppressWarnings("unchecked")
	public void setSession(Session s) {
		this.session = s;
	}

	protected Session getSession() {
			if (session == null)
					throw new IllegalStateException("Session has not been set on DAO before usage");
			return session;
	}


}
