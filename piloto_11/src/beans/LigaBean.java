/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import dao.DAOFactory;
import domain.Equipo;

public class LigaBean {

	
	private int numJornadas;//=38; # no hace falta inicializarla, esta en el faces.config
	public int getNumJornadas(){return numJornadas;}
	public void setNumJornadas(int numJornadas){this.numJornadas = numJornadas;}


	private String[] jornadas;
	public String[] getJornadas() {
		if(jornadas==null){
		   jornadas=new String[numJornadas];
		   for(int i=0;i<numJornadas;i++) jornadas[i]=String.valueOf(i+1);
		}
		return jornadas;
	}

	public void setJornadas(String[] jornadas) {
		this.jornadas = jornadas;
	}


	private List<SelectItem> equipos;
	
	public List<SelectItem> getEquipos(){
		if(equipos==null){
			List<Equipo> lista= DAOFactory.getEquipoDAO().findAll();
			equipos= new ArrayList<SelectItem>();
			for(Equipo e: lista){
				equipos.add(new SelectItem(e.getIdEquipo(),e.getNombre()));
			}
		}
		return equipos;
	}
	


}
