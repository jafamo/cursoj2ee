package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import dao.DAOFactory;
import domain.Equipo;

public class StringToEquipoConverter implements Converter {

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String id) {
		// TODO Auto-generated method stub
		Equipo e=DAOFactory.getEquipoDAO().findById(Integer.parseInt(id));
		return e;
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		// TODO Auto-generated method stub
		return arg2.toString();
	}

}
