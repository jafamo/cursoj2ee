package dao;

import dao.utils.HibernateUtil;


public class DAOFactory {
	
	public static PartidoDAO getPartidoDAO(){
		return new PartidoDAOImpl(HibernateUtil.getSession());
	}
	
	public static EquipoDAO getEquipoDAO(){ 
		return new EquipoDAOImpl(HibernateUtil.getSession());
	}
}
