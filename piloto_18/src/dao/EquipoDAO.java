package dao;

import java.util.List;

import domain.Equipo;
import domain.Partido;

public interface EquipoDAO {    
		
		Equipo findById(Integer id);
	    
	    //List<Equipo> findByJornada(String jornada);
	    
	    List<Equipo> findByEquipo(String equipo);
	 
	    List<Equipo> findAll();
	    
	    List<Equipo> findBySimilarEquipo(String nombre);
	 
	    void save(Equipo entity);
	 
	    void remove(Equipo entity);		
	    
	}

