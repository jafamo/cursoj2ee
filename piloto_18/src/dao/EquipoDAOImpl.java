package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import dao.utils.GenericHibernateDAO;
import domain.Equipo;
import domain.Partido;


class EquipoDAOImpl extends GenericHibernateDAO implements EquipoDAO{

	public EquipoDAOImpl(Session s) {
		super(s);
	}
	
	@SuppressWarnings("unchecked")
	public Equipo findById(Integer id) {
		Equipo entity;
		entity = (Equipo) getSession().get(Equipo.class, id);
		return entity;
	}
	
	@SuppressWarnings("unchecked")
	public List<Equipo> findByEquipo(String equipo) {
		Query q = getSession().createQuery("From Equipo q " +
										   "where q.nombre='"+equipo+"'");
		return q.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Equipo> findAll() {
		Query q = getSession().createQuery("From Equipo");
		return q.list();
	}
	
	@SuppressWarnings("unchecked")	
	public List<Equipo> findBySimilarEquipo(String nombre){
		Query q = getSession().createQuery("From Equipo q where lower(q.nombre) like ?");		
		q.setParameter(0, nombre.toLowerCase()+"%");		
		return q.list();
	}
	
	@SuppressWarnings("unchecked")
	public void save(Equipo entity) {
		getSession().saveOrUpdate(entity);
	}

	public void remove(Equipo entity) {
		getSession().delete(entity);
	}
}
