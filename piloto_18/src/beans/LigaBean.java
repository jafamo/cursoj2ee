/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;

import dao.DAOFactory;
import domain.Equipo;

@ManagedBean
@RequestScoped
public class LigaBean {

	private String[] jornadas;
	private int numJornadas=38;
	
	public String[] getJornadas() {
		if(jornadas==null){
		   jornadas=new String[numJornadas];
		   for(int i=0;i<numJornadas;i++) jornadas[i]=String.valueOf(i+1);
		}
		return jornadas;
	}

	public void setJornadas(String[] jornadas) {
		this.jornadas = jornadas;
	}


	private List<SelectItem> equipos;
	
	public List<SelectItem> getEquipos(){
		if(equipos==null){
			List<Equipo> lista= DAOFactory.getEquipoDAO().findAll();
			equipos= new ArrayList<SelectItem>();
			for(Equipo e: lista){
				equipos.add(new SelectItem(e.getIdEquipo(),e.getNombre()));
			}
		}
		return equipos;
	}
	
	private String equipo;
	public String getEquipo() {return equipo;}
	public void setEquipo(String equipo) {this.equipo = equipo;}
	
	private String estadioLocation;
	public void setEstadioLocation(String estadioLocation){this.estadioLocation = estadioLocation;}
	public String getEstadioLocation() {
		if(equipo.toLowerCase().indexOf("valencia")>=0) return "39.4745215,-0.3581571";
		if(equipo.toLowerCase().indexOf("barcelona")>=0) return "41.381542, 2.122893";
		if(equipo.toLowerCase().indexOf("real madrid")>=0) return "40.4530417,-3.6882944";
		if(equipo.toLowerCase().indexOf("levante")>=0) return "39.4947222,-0.3641667";
		if(equipo.toLowerCase().indexOf("villareal")>=0) return "39.9439906,-0.1032031";
		if(equipo.toLowerCase().indexOf("betis")>=0) return "37.3564779,-5.9817635";
		if(equipo.toLowerCase().indexOf("at. de madrid")>=0) return "40.4008758,-3.7206801";
		if(equipo.toLowerCase().indexOf("sevilla")>=0) return "37.3832844,-5.9754693";
		return null;
	}
	
	


}
