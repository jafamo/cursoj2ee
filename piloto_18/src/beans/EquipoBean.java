/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import dao.DAOFactory;
import dao.utils.HibernateUtil;
import domain.Equipo;

@ManagedBean
@SessionScoped
public class EquipoBean {


	/**** AJAX LISTENERS ***********************************/
	
		
	    private Equipo equip;
	    public Equipo getEquip(){return equip;}
		public void setEquip(Equipo equip){this.equip = equip;}
	    
	    private String equipo;
	    public String getEquipo(){return equipo;}
		public void setEquipo(String equipo){this.equipo = equipo;}
	   
		private String direccion;
		public String getDireccion() {return direccion;}
		public void setDireccion(String direccion) {this.direccion = direccion;}
		
		private Integer idEquipo;
		public Integer getidEquipo() {return idEquipo;}
		public void setidEquipo(Integer idEquipo) {this.idEquipo = idEquipo;}
		
		 
		public void filtroEquipo(){
	    	this.filtraPorEquipo();
	    }
		
		private void filtraPorEquipo(){
	    	if(equipo.trim().length()==0)
	    		listaDeEquipos=DAOFactory.getEquipoDAO().findAll();
	    	else
	    		listaDeEquipos=DAOFactory.getEquipoDAO().findBySimilarEquipo(equipo);
	    }
	
	/**** AJAX LISTENERS ***********************************/
		public String guardaEquipo(){
			//String equi=equipo.getNombre();			
			   guardar();
			   return "listado";			
			
		}
	
	   
	private void guardar(){
		   HibernateUtil.beginTransaction();
		   DAOFactory.getEquipoDAO().save(equip);
		   HibernateUtil.endTransaction();
		   dirty=true;
	}
	
	public void eliminar(ActionEvent e){

		HibernateUtil.beginTransaction();
		Equipo p=DAOFactory.getEquipoDAO().findById(Integer.valueOf(selectedEquipo));
		DAOFactory.getEquipoDAO().remove(p);
		HibernateUtil.endTransaction();
		
		selectedEquipo=null;
		dirty=true;
	}
	
	private String selectedEquipo;
	public String getSelectedEquipo(){return selectedEquipo;	}
	public void setSelectedEquipo(String selectedEquipo){this.selectedEquipo = selectedEquipo;}
	
	
	public void actualizar(){
		if(selectedEquipo==null)
			crearEquipo();
		else{
			equip=DAOFactory.getEquipoDAO().findById(Integer.valueOf(selectedEquipo));
		}
	}
	    
    private void crearEquipo(){
    	equip=new Equipo();
    }
    /*******************************************************************/
    
     


    private List<Equipo> listaDeEquipos;
    private boolean dirty=false;
    public List<Equipo> getListaDeEquipos() {
        if(listaDeEquipos==null || dirty==true){
            listaDeEquipos=DAOFactory.getEquipoDAO().findAll();
            dirty=false;
            selectedEquipo=null;
        }
        return listaDeEquipos;
    }
    public void setListaDeEquipos(List<Equipo> equipos) {
        this.listaDeEquipos = equipos;
    }


    

}
