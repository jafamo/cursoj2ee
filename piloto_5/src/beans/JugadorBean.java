package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import dao.DAOFactory;
import domain.Equipo;
import domain.Jugador;

public class JugadorBean {

	List<Jugador> jugadores;
	
	
	public void filtarPorEquipo(ActionEvent event){
		String selected=(String)FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectedEquipo");
		//recuperamos el equipo con este ID
		Equipo e=DAOFactory.getEquipoDAO().findById(Integer.valueOf(selected));
		
		
		//cerramos la sesion al ser un lazy
		
		DAOFactory.getEquipoDAO().closeSession();
		//recuperar los juadores
		jugadores=new ArrayList(e.getJugadors());
		
	}
	
	public List<Jugador> getJugadores(){
		return jugadores;
	}
}
