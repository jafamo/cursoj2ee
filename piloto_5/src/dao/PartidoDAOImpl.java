package dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import dao.utils.GenericHibernateDAO;
import domain.Partido; 


public class PartidoDAOImpl implements PartidoDAO {

	private static Session session=null;
	
	public PartidoDAOImpl(){
		if(session==null){
			createSession();
		}
	}
	
	private void createSession(){
		Configuration conf= new Configuration().configure();
		
		StandardServiceRegistryBuilder sb = new StandardServiceRegistryBuilder();
	    sb.applySettings(conf.getProperties());
	    StandardServiceRegistry standardServiceRegistry = sb.build();                   
	    SessionFactory sf = conf.buildSessionFactory(standardServiceRegistry);

		session=sf.openSession();
	}


	@SuppressWarnings("unchecked")
	public Partido findById(Integer id) {
		Partido entity;
		entity = (Partido) session.get(Partido.class, id);
		return entity;
	}

	
	
	
	@SuppressWarnings("unchecked")
	public List<Partido> findAll() {
		/*Query q = session.createQuery("From Partido");
		return q.list();*/
		
		/*Criteria crit = session.createCriteria(Partido.class);
		return crit.list();*/
		
		Query q = session.getNamedQuery("all");
		return q.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Partido> findByJornada(String jornada) {
		/*Query q=session.createQuery("From Partido where jornada="+jornada);
		return q.list();*/
		
		/*Criteria crit = session.createCriteria(Partido.class);
		crit.add(Restrictions.eq("jornada", Integer.parseInt(jornada)));
		return crit.list();*/
		
		Query q = session.getNamedQuery("byJornada");
		q.setInteger(0, Integer.valueOf(jornada));
		return q.list();
	}


	@SuppressWarnings("unchecked")
	public List<Partido> findByEquipo(String equipo) {
		/*Query q = session.createQuery("From Partido p " +
										   "where " +
										   "lower(p.equipoByIdEquipoLocal.nombre)='"+equipo.toLowerCase()+
										   "' or lower(p.equipoByIdEquipoVisitante.nombre)='"+equipo.toLowerCase()+"'");
		return q.list();*/
		
		/*Criteria crit= session.createCriteria(Partido.class);
		crit.createAlias("equipoByIdEquipoLocal", "local");
		crit.createAlias("equipoByIdEquipoVisitante", "visitante");
		crit.add(Restrictions.or(Restrictions.eq("local.nombre", equipo).ignoreCase(), 
				Restrictions.eq("visitante.nombre", equipo).ignoreCase()));
		
		return crit.list();*/
		
		Query q = session.getNamedQuery("byEquipo");
		q.setString("eq", equipo.toLowerCase());
		return q.list();
			
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public void save(Partido entity) {
		Transaction tx=session.beginTransaction();
		session.saveOrUpdate(entity);
		tx.commit();
	}
 

	public void remove(Partido entity) {
		Transaction tx=session.beginTransaction();
		session.delete(entity);
		tx.commit();
	}


	// Getters and Setters
	public Session getSession(){
		return session;
	}
	
	public static void setSession(Session s){
		session=s;
	}


}
