package converters;

import java.text.SimpleDateFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import domain.Partido;

public class PartidoToStringConverter implements Converter {

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		return null;
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		// TODO Auto-generated method stub
		Partido p=(Partido)arg2;
		
		String desc=p.getEquipoLocal().getNombre()+"-"+p.getEquipoVisitante().getNombre()+"<br>";
		if(p.getFecha()!=null){
			SimpleDateFormat formater=new SimpleDateFormat("dd/MM/yyyy");
			desc=desc+"Jornada: "+p.getJornada()+" (" + formater.format(p.getFecha()) +")<br>";
		}
		else
			desc=desc+"Jornada: "+p.getJornada()+"<br>";
		if(p.getGolesLocal()==null || p.getGolesVisitante()==null)
			desc=desc+"Partido por jugar";
		else
			desc=desc+"Resultado: "+p.getGolesLocal()+"-"+p.getGolesVisitante();
		if(p.getObservaciones()!=null && p.getObservaciones().length()==0) desc=desc+"<br>Observaciones: "+p.getObservaciones()+"-"+p.getGolesVisitante();
		
		return desc;
	}

}
