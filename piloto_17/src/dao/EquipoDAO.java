package dao;

import java.util.List;

import domain.Equipo;

public interface EquipoDAO {
    
	    List<Equipo> findAll();
    
	    Equipo findById(Integer id);
	    
	}

