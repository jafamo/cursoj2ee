/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import dao.DAOFactory;
import dao.utils.HibernateUtil;
import domain.Partido;

@ManagedBean
@SessionScoped
public class PartidoBean {



	// TODO: Implementar AjaxListener
	// Listener filtroJornada --> llama a this.filtraPorJornada();
	// Listener filtroEquipo --> llama a this.filtraPorEquipo();
	
		public void filtroJornada(AjaxBehaviorEvent e) {
			filtraPorJornada();
		}
		
		public void filtroEquipo(AjaxBehaviorEvent e) {
			filtraPorEquipo();
		}
	
	
	
		private String jornada;
		public String getJornada() {return jornada;}
		public void setJornada(String jornada) {this.jornada = jornada;}
		
	    private String equipo;
	    public String getEquipo(){return equipo;}
		public void setEquipo(String equipo){this.equipo = equipo;}
	
	    private void filtraPorJornada(){
	    	if(jornada.equals("Todas"))
	    		listaDePartidos=DAOFactory.getPartidoDAO().findAll();
	    	else
	    		listaDePartidos=DAOFactory.getPartidoDAO().findByJornada(jornada);
	    }
	    
	    private void filtraPorEquipo(){
	    	if(equipo.trim().length()==0)
	    		listaDePartidos=DAOFactory.getPartidoDAO().findAll();
	    	else
	    		listaDePartidos=DAOFactory.getPartidoDAO().findBySimilarEquipo(equipo);
	    }
	
	/**** AJAX LISTENERS ***********************************/

	
	
	public String guardaPartido(){
		String equipoLocal=partido.getEquipoLocal().getNombre();
		String equipoVisitante=partido.getEquipoVisitante().getNombre();

		if(!equipoLocal.equals(equipoVisitante)){
		   guardar();
		   return "listado";
		}
		else{
		   return "error";
		}
	}
	   
	private void guardar(){
		   HibernateUtil.beginTransaction();
		   DAOFactory.getPartidoDAO().save(partido);
		   HibernateUtil.endTransaction();
		   dirty=true;
	}
	
	public void eliminar(ActionEvent e){
		String partidoID=null;	
			
		partidoID=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectedPartido");
		
		
		HibernateUtil.beginTransaction();
		Partido p=DAOFactory.getPartidoDAO().findById(Integer.valueOf(partidoID));
		DAOFactory.getPartidoDAO().remove(p);
		HibernateUtil.endTransaction();
		
		dirty=true;
	}
	
	String selectedPartido;
	public String getSelectedPartido(){return selectedPartido;	}
	public void setSelectedPartido(String selectedPartido){this.selectedPartido = selectedPartido;}
	
	
	public void actualizar(){
		if(selectedPartido==null)
			crearPartido();
		else{
			partido=DAOFactory.getPartidoDAO().findById(Integer.valueOf(selectedPartido));
		}
	}
	    
    private void crearPartido(){
    	partido=new Partido();
    }
    /*******************************************************************/
    
     
  
	private Partido partido;
	public Partido getPartido() {
        return partido;
    }
    public void setPartido(Partido partido) {
        this.partido = partido;
    }


    private List<Partido> listaDePartidos;
    private boolean dirty=false;
    public List<Partido> getListaDePartidos() {
        if(listaDePartidos==null || dirty==true){
            listaDePartidos=DAOFactory.getPartidoDAO().findAll();
            dirty=false;
        		selectedPartido=null;
        }
        return listaDePartidos;
    }
    public void setListaDePartidos(List<Partido> partidos) {
        this.listaDePartidos = partidos;
    }


    private boolean porJugar;
	public boolean isPorJugar() {
		return porJugar;
	}
	public void setPorJugar(boolean porJugar) {
		this.porJugar = porJugar;
	}
    

}
