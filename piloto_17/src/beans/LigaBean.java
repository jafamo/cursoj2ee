/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;

import dao.DAOFactory;
import domain.Equipo;

@ManagedBean
@RequestScoped
public class LigaBean {

	private String[] jornadas;
	private int numJornadas=38;
	
	public String[] getJornadas() {
		if(jornadas==null){
		   jornadas=new String[numJornadas];
		   for(int i=0;i<numJornadas;i++) jornadas[i]=String.valueOf(i+1);
		}
		return jornadas;
	}

	public void setJornadas(String[] jornadas) {
		this.jornadas = jornadas;
	}


	private List<SelectItem> equipos;
	
	public List<SelectItem> getEquipos(){
		if(equipos==null){
			List<Equipo> lista= DAOFactory.getEquipoDAO().findAll();
			equipos= new ArrayList<SelectItem>();
			for(Equipo e: lista){
				equipos.add(new SelectItem(e.getIdEquipo(),e.getNombre()));
			}
		}
		return equipos;
	}
	


}
