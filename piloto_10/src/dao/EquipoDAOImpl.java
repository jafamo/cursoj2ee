package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import dao.utils.GenericHibernateDAO;
import domain.Equipo;

class EquipoDAOImpl extends GenericHibernateDAO implements EquipoDAO{

	public EquipoDAOImpl(Session s) {
		super(s);
	}
	
	@SuppressWarnings("unchecked")
	public Equipo findById(Integer id) {
		Equipo entity;
		entity = (Equipo) getSession().get(Equipo.class, id);
		return entity;
	}

	@SuppressWarnings("unchecked")
	public List<Equipo> findAll() {
		Query q = getSession().createQuery("From Equipo");
		return q.list();
	}
}
