/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import dao.DAOFactory;
import dao.utils.HibernateUtil;
import domain.Equipo;
import domain.Partido;

@ManagedBean
@SessionScoped
public class PartidoBean {

    /*******************************************************************/
	/*************************TODO**************************************/
	/*******************************************************************/

	private HtmlSelectOneMenu equipoLocalComponent;
	public HtmlSelectOneMenu getEquipoLocalComponent(){return equipoLocalComponent;}
	public void setEquipoLocalComponent(HtmlSelectOneMenu equipoLocalComponent){this.equipoLocalComponent = equipoLocalComponent;}

	private HtmlSelectOneMenu equipoVisitanteComponent;
	public HtmlSelectOneMenu getEquipoVisitanteComponent(){return equipoVisitanteComponent;}
	public void setEquipoVisitanteComponent(HtmlSelectOneMenu equipoVisitanteComponent){this.equipoVisitanteComponent = equipoVisitanteComponent;}
	

	
	
	public String guardaPartido(){
		Equipo equipoLocal=null;
		Equipo equipoVisitante=null;
		
		// Guardamos en equipoLocal y equipoVisitante los objetos seleccionados
		// Utilizamos los componentes gráficos enlazados
		// Método getValue()
		equipoLocal = (Equipo)equipoLocalComponent.getValue();
		equipoVisitante = (Equipo) equipoVisitanteComponent.getValue();
		
		if(!equipoLocal.getNombre().equals(equipoVisitante.getNombre())){
		   partido.setEquipoLocal(equipoLocal);
		   partido.setEquipoVisitante(equipoVisitante);
		   guardar();
		   return "listado";
		}
		else{
		   return "error";
		}
	}
	   
	private void guardar(){
		   HibernateUtil.beginTransaction();
		   DAOFactory.getPartidoDAO().save(partido);
		   HibernateUtil.endTransaction();
		   dirty=true;
	}
	/*******************************************************************/
	
	
	
	
	
	public void eliminar(ActionEvent e){
		String partidoID=null;	
			
		/******** Recoger el par�metro selectedParam y guardarlo en partidoID************************/
		partidoID=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectedPartido");
		
		
		HibernateUtil.beginTransaction();
		Partido p=DAOFactory.getPartidoDAO().findById(Integer.valueOf(partidoID));
		DAOFactory.getPartidoDAO().remove(p);
		HibernateUtil.endTransaction();
		
		dirty=true;
	}
	
	String selectedPartido;
	public String getSelectedPartido(){return selectedPartido;	}
	public void setSelectedPartido(String selectedPartido){this.selectedPartido = selectedPartido;}
	
	
	public void actualizar(){
		if(selectedPartido==null)
			crearPartido();
		else{
			partido=DAOFactory.getPartidoDAO().findById(Integer.valueOf(selectedPartido));
		}

		this.equipoLocalComponent.setSubmittedValue(partido.getEquipoLocal());
		this.equipoVisitanteComponent.setSubmittedValue(partido.getEquipoVisitante());

	}
	    
    private void crearPartido(){
    	partido=new Partido();
    }
    /*******************************************************************/
    
    public void filtroJornada(ValueChangeEvent ve){
    	this.filtraPorJornada((String)ve.getNewValue());
    }
    
    private void filtraPorJornada(String jornada){
    	if(jornada.equals("Todas"))
    		listaDePartidos=DAOFactory.getPartidoDAO().findAll();
    	else
    		listaDePartidos=DAOFactory.getPartidoDAO().findByJornada(jornada);
    }
    
    
    public void filtroEquipo(ActionEvent e){
    	this.filtraPorEquipo();
    }
    
    private void filtraPorEquipo(){
    	if(equipo.trim().length()==0)
    		listaDePartidos=DAOFactory.getPartidoDAO().findAll();
    	else
    		listaDePartidos=DAOFactory.getPartidoDAO().findByEquipo(equipo);
    }
        
    public String equipo;
    public String getEquipo(){return equipo;}
	public void setEquipo(String equipo){this.equipo = equipo;}

  
	private Partido partido;
	public Partido getPartido() {
        return partido;
    }
    public void setPartido(Partido partido) {
        this.partido = partido;
    }


    private List<Partido> listaDePartidos;
    private boolean dirty=false;
    public List<Partido> getListaDePartidos() {
        if(listaDePartidos==null || dirty==true){
            listaDePartidos=DAOFactory.getPartidoDAO().findAll();
            dirty=false;
            selectedPartido=null;
        }
        return listaDePartidos;
    }
    public void setListaDePartidos(List<Partido> partidos) {
        this.listaDePartidos = partidos;
    }


    private boolean porJugar;
	public boolean isPorJugar() {
		return porJugar;
	}
	public void setPorJugar(boolean porJugar) {
		this.porJugar = porJugar;
	}
    

}
