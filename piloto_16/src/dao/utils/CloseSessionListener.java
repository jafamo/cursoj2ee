package dao.utils;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class CloseSessionListener implements PhaseListener {

	public void afterPhase(PhaseEvent event) {
		if(HibernateUtil.getSession()!=null) HibernateUtil.closeSession();
	}

	public void beforePhase(PhaseEvent event) {
		// TODO Auto-generated method stub
	}

	public PhaseId getPhaseId() {
		// TODO Auto-generated method stub
		return PhaseId.RENDER_RESPONSE;
	}

}
