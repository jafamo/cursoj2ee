# Curso Online de Desarrollo JEE con JSF, HIBERNATE y SPRING





## Introducción al desarrollo JEE
◦ Aspectos básicos de la arquitectura

◦ Configuración del entorno de desarrollo


## Hibernate
◦ Introducción y primeros 

◦ Ingeniería Inversa

◦ El API de hibernate

◦ Hibernate Tools

◦ Mappings y Asociaciones

◦ Patrón DAO


## Java Server Faces
◦ Introducción

◦ Modelo de componentes de interfaz de usuario

◦ Modelo de Navegación

◦ Desarrollo y configuración de Managed Beans

◦ Enlace de listas con Beans

◦ Gestión de Eventos

◦ Conversores

◦ Validadores

◦ Componentes immediate y el ciclo de vida

◦ Renderizado de errores


## Java Server Faces
◦ Paso de Parámetros

◦ Navegación Dinámica

◦ Inicialización de Atributos Manejados

(Managed Properties)

◦ Enlace con Componentes Gráficos

◦ Resource Bundles e Internacionalización

◦ Diseños de Layouts con Facelets

◦ Desarrollo de Interfaces Ajax en JSF

◦ Librerías externas: PrimeFaces


## Spring
◦ Introducción

◦ Inversión de Control (IoC)

◦ Programación Orientada a Aspectos (AOP)

◦ Integración JSF-Hibernate-Spring



*Proyecto realizado por javier Farinós*
