package dao.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


public class HibernateUtil {
		private static SessionFactory sf;
		private static Session session;
		private static Transaction tx;
		
		public static Session getSession(){
			if(session==null) 
				session=getSessionFactory().openSession();
			return session;
		}
		
		public static void closeSession(){
			session.close();
			session=null;
		}
		
		public static void beginTransaction(){
			if(tx==null) tx=getSession().beginTransaction();
		}
		
		public static void endTransaction(){
			tx.commit();
			tx=null;
		}
		
		private static SessionFactory getSessionFactory(){
			if(sf==null){
				Configuration conf= new Configuration().configure();
				
				StandardServiceRegistryBuilder sb = new StandardServiceRegistryBuilder();
			    sb.applySettings(conf.getProperties());
			    StandardServiceRegistry standardServiceRegistry = sb.build();                   
			    sf = conf.buildSessionFactory(standardServiceRegistry);
			}
			return sf;

		}
		
	}
