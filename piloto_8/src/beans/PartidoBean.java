/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import dao.DAOFactory;
import domain.Partido;

@ManagedBean
@SessionScoped
public class PartidoBean {

    private Partido partido;

    public Partido getPartido() {
        return partido;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }



    private List<Partido> listaDePartidos;

    public List<Partido> getListaDePartidos() {
        if(listaDePartidos==null){
            listaDePartidos=DAOFactory.getPartidoDAO().findAll();
         }
        return listaDePartidos;
    }

    public void setListaDePartidos(List<Partido> partidos) {
        this.listaDePartidos = partidos;
    }



    private boolean porJugar;

	public boolean isPorJugar() {
		return porJugar;
	}

	public void setPorJugar(boolean porJugar) {
		this.porJugar = porJugar;
	}
    

}
