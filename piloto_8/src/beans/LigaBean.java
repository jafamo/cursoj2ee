/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import dao.DAOFactory;
import domain.Equipo;

@ManagedBean
@RequestScoped
public class LigaBean {

	private String[] jornadas;
	private int numJornadas=38;
	
	public String[] getJornadas() {
		if(jornadas==null){
		   jornadas=new String[numJornadas];
		   for(int i=0;i<numJornadas;i++) jornadas[i]=String.valueOf(i+1);
		}
		return jornadas;
	}

	public void setJornadas(String[] jornadas) {
		this.jornadas = jornadas;
	}


	private List<Equipo> equipos;

	public List<Equipo> getEquipos() {
		if(equipos==null){
			equipos=DAOFactory.getEquipoDAO().findAll();
		}
		return equipos;
	}

	public void setEquipos(List<Equipo> equipos) {
		this.equipos = equipos;
	}
	


}
